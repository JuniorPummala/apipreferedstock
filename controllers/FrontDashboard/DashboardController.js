const jwt = require('jsonwebtoken');
const expressJwt = require('express-jwt');
const config = require('./../../config');
const { validationResult } = require('express-validator/check');
const bcrypt = require('bcrypt');
const nodemailer = require('nodemailer');
var moment = require('moment-timezone');
var momentT = require('moment');
const sendgridTransport = require('nodemailer-sendgrid-transport');
const sgMail = require('@sendgrid/mail');
const {
  GET_TRANSACTION_TOPUP,
  GET_ALLUSER_ADMIN,
  GET_USER_DASHBOARD,
  GET_USER,
  GET_TRANSACTION_INVEST,
  GET_BROKER_ADMIN,
  PENDING_DIVIDENT,
  APPROVE_DIVIDEND_CFO,
  APPROVE_DIVIDEND_CEO,
  GET_TRANSACTION_WITHDRAW,
  UPDATE_SLIP_WH,
  GET_TRANSACTION_COMMISSION,
  APPROVE_COMMISSION_CFO,
  APPROVE_COMMISSION_CEO,
  APPROVE_WITHDRAW_CFO,
  APPROVE_WITHDRAW_CEO,
  QUERY_TOPUP_ADMIN,
  APPROVE_TOPUP_CEO,
  APPROVE_TOPUP_CFO
} = require('./DashBoardSQL.json');
const { resp } = require('../../helper/response');
const { connect } = require('pm2');
const connection = require('express-myconnection');
const authCheck = require('../../helper/getId');
require('dotenv').config();

const aws = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');

var omise = require('omise')({
  publicKey: process.env.PIBLIC_KEY_OMISE,
  secretKey: process.env.SECRET_KEY_OMISE
});

const transporter = nodemailer.createTransport(
  sendgridTransport({
    auth: {
      api_key: process.env.EMAIL_API_KEY
    }
  })
);

sgMail.setApiKey(process.env.EMAIL_API_KEY);

const spacesEndpoint = new aws.Endpoint(process.env.BUCKET_ENDPOINT);

const s3 = new aws.S3({
  endpoint: spacesEndpoint,
  accessKeyId: process.env.BUCKET_KEY,
  secretAccessKey: process.env.BUCKET_SECREET
});

exports.getUserDashboard = async (req, res, next) => {
  try {
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      await authCheck.getUserId(
        req.headers.authorization,
        async (id, role, approve) => {
          var sql = GET_USER_DASHBOARD.code;
          await connection.query(sql, [id], async (err, result) => {
            if (err) {
              return res.status(200).send(resp(false, result, '', ''));
            } else {
              return res.status(200).send(resp(true, result, '', ''));
            }
          });
        }
      );
    });
  } catch (err) {
    console.log(err);
  }
};

exports.getUser = async (req, res, next) => {
  try {
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      await authCheck.getUserId(
        req.headers.authorization,
        async (id, role, approve) => {
          var sql = GET_USER.code;
          await connection.query(sql, [id], async (err, result) => {
            if (err) {
              return res.status(200).send(resp(false, result[0], '', ''));
            } else {
              return res.status(200).send(resp(true, result[0], '', ''));
            }
          });
        }
      );
    });
  } catch (err) {
    console.log(err);
  }
};

exports.amountStock = async (req, res, next) => {
  try {
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      await authCheck.getUserId(
        req.headers.authorization,
        async (id, role, approve) => {
          var sql = GET_TRANSACTION_INVEST.code;
          await connection.query(sql, [id], async (err, result) => {
            if (err) {
              return res.status(200).send(resp(false, result, '', ''));
            } else {
              return res.status(200).send(resp(true, result, '', ''));
            }
          });
        }
      );
    });
  } catch (err) {
    console.log(err);
  }
};

exports.transactionTopupDashboard = async (req, res, next) => {
  try {
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      await authCheck.getUserId(
        req.headers.authorization,
        async (id, role, approve) => {
          var sql = GET_TRANSACTION_TOPUP.code;
          await connection.query(sql, [id], async (err, result) => {
            if (err) {
              return res.status(200).send(resp(false, result, '', ''));
            } else {
              return res.status(200).send(resp(true, result, '', ''));
            }
          });
        }
      );
    });
  } catch (err) {
    console.log(err);
  }
};

exports.getDataADMIN = async (req, res, next) => {
  try {
    console.log('type : ', req.params.type);
    if (req.params.type == 0) {
      await req.getConnection(async (err, connection) => {
        if (err) return next(err);
        var sql = GET_ALLUSER_ADMIN.code;
        await connection.query(sql, async (err, result) => {
          if (err) {
            return res.status(200).send(resp(false, result, '', ''));
          } else {
            return res.status(200).send(resp(true, result, '', ''));
          }
        });
      });
    } else if (req.params.type == 1) {
      //broker
      await req.getConnection(async (err, connection) => {
        if (err) return next(err);
        var sql = GET_BROKER_ADMIN.code;
        await connection.query(sql, async (err, result) => {
          if (err) {
            return res.status(200).send(resp(false, result, '', ''));
          } else {
            return res.status(200).send(resp(true, result, '', ''));
          }
        });
      });
    } else if (req.params.type == 2) {
      //pending divident
      await req.getConnection(async (err, connection) => {
        if (err) return next(err);
        var sql = PENDING_DIVIDENT.code;
        await connection.query(sql, async (err, result) => {
          if (err) {
            return res.status(200).send(resp(false, result, '', ''));
          } else {
            return res.status(200).send(resp(true, result, '', ''));
          }
        });
      });
    } else if (req.params.type == 3) {
      await req.getConnection(async (err, connection) => {
        if (err) return next(err);
        var sql = GET_TRANSACTION_WITHDRAW.code;
        connection.query(sql, async (err, result) => {
          if (err) {
            return res.status(200).send(resp(false, result, '', ''));
          } else {
            let results = [];
            for (let i = 0; i < result.length; i++) {
              let objs = [];
              let data = {
                idtransactionWithdraw: result[i].idtransactionWithdraw,
                No: result[i].No,
                idUser: result[i].idUser,
                amount: result[i].amount,
                BankName: result[i].BankName,
                AccountName: result[i].AccountName,
                AccountNumber: result[i].AccountNumber,
                SwiftCode: result[i].SwiftCode,
                DateWithDraw: result[i].DateWithDraw,
                slip: result[i].slip,
                CEO: result[i].CEO,
                CFO: result[i].CFO,
                SystemName: result[i].SystemName,
                KYC: {
                  AccountNumber: result[i].AccountNumberKYC,
                  BankName: result[i].BankNameKYC,
                  AccountName: result[i].AccountNameKYC,
                  SwiftCode: result[i].SwiftCodeKYC
                }
              };
              results.push(data);
            }
            return res.status(200).send(resp(true, results, '', ''));
          }
        });
      });
      //   GET_TRANSACTION_COMMISSION
    } else if (req.params.type == 4) {
      await req.getConnection(async (err, connection) => {
        if (err) return next(err);
        var sql = GET_TRANSACTION_COMMISSION.code;
        await connection.query(sql, async (err, result) => {
          if (err) {
            return res.status(200).send(resp(false, result, '', ''));
          } else {
            return res.status(200).send(resp(true, result, '', ''));
          }
        });
      });
    } else if (req.params.type == 5) {
      await req.getConnection(async (err, connection) => {
        if (err) return next(err);
        var sql = QUERY_TOPUP_ADMIN.code;
        await connection.query(sql, async (err, result) => {
          if (err) {
            return res.status(200).send(resp(false, result, '', ''));
          } else {
            return res.status(200).send(resp(true, result, '', ''));
          }
        });
      });
    }
    // QUERY_TOPUP_ADMIN
  } catch (err) {
    console.log(err);
  }
};

const uploadSlip = multer({
  storage: multerS3({
    s3: s3,
    bucket: process.env.BUCKET,
    key: function (req, file, cb) {
      cb(
        null,
        'SlipWithdraw' + '/' + new Date().getTime() + '_' + file.originalname
      );
    }
  })
}).single('image');

exports.UpdateWithDraw = (req, res, next) => {
  uploadSlip(req, res, function (err) {
    var { body } = req;
    console.log('UpdateWithDraw', body);
    let idtransactionWithdraw = body.idtransactionWithdraw;
    if (err) {
      res.status(400).json({
        success: 'error',
        message: err.message
      });
    } else {
      let path = req.file.key;
      console.log('path', path);
      req.getConnection((err, connection) => {
        if (err) return next(err);
        var sql = UPDATE_SLIP_WH.code;
        connection.query(sql, [path, idtransactionWithdraw], (err, result) => {
          if (err) {
            return res
              .status(200)
              .send(resp(false, result, '', 'อัพโหลดรูปเรียบร้อย'));
          } else {
            return res
              .status(200)
              .send(resp(true, result, '', 'อัพโหลดรูปเรียบร้อย'));
          }
        });
      });
    }
  });
};

exports.getImageSlip = (req, res, next) => {
  //   console.log('sasdc : ', req.params.path + '/' + req.params.path2);
  var getParams = {
    Bucket: process.env.BUCKET,
    Key: req.params.path + '/' + req.params.path2
  };
  s3.getObject(getParams, async function (err, data) {
    if (err) {
      if (err) return next(err);
    } else {
      // res.status(200).send(resp(true, data.body, null, null));
      res.writeHead(200, {
        'Content-Type': 'image/png'
      });
      res.write(data.Body, 'binary');
      res.end(null, 'binary');
    }
  });
};

exports.getCommissionBroker = (req, res, next) => {
  try {
    req.getConnection(async (err, connection) => {
      if (err) return next(err);
      authCheck.getUserId(
        req.headers.authorization,
        async (id, role, approve) => {
          var getIDBroker = `SELECT * FROM AmazieFintechP1.broker where userID = ${id}`;
          connection.query(getIDBroker, [], (err, result) => {
            var idBroker = result[0].idbroker;
            if (err) {
              return res.status(200).send(resp(false, result, '', ''));
            } else {
              var getcomm = `SELECT Commission FROM AmazieFintechP1.transactionCommission where BrokerID = ${idBroker} AND CEO=1 AND CFO=1;`;
              connection.query(getcomm, [], (err, results) => {
                if (err) {
                  return res.status(200).send(resp(false, null, '', ''));
                } else {
                  let index = [];
                  let commission = 0;
                //   console.log('getCommissionBroker : ,', results);
                  for (let i = 0; i < results.length; i++) {
                    commission = commission + results[1].Commission;
                  }
                  index.push(commission);
                  return res.status(200).send(
                    resp(
                      true,
                      {
                        Commission: commission,
                        BrokerID: idBroker
                      },
                      '',
                      ''
                    )
                  );
                }
              });
            }
          });
        }
      );
    });
  } catch (err) {}
};

exports.AllUserNetworkBB = (req, res, next) => {
  try {
    req.getConnection(async (err, connection) => {
      if (err) return next(err);
      authCheck.getUserId(
        req.headers.authorization,
        async (id, role, approve) => {
          var getIDBroker = `SELECT * FROM AmazieFintechP1.broker where userID = ${id}`;
          connection.query(getIDBroker, [], (err, result) => {
            var idBroker = result[0].idbroker;
            if (err) {
              return res.status(200).send(resp(false, result, '', ''));
            } else {
              var getAllUser = `SELECT id, concat(FirstName, ' ', LastName) AS Name , Email, Phone, share, active, TimeApprove FROM AmazieFintechP1.user where RefID = ${idBroker}`;
              connection.query(getAllUser, [], (err, result2) => {
                if (err) {
                  return res.status(200).send(resp(false, result2, '', ''));
                } else {
                  //   console.log('result2 : ', result2);
                  return res
                    .status(200)
                    .send(
                      resp(
                        true,
                        { result2, UserNetwork: result2.length },
                        '',
                        ''
                      )
                    );
                }
              });
            }
          });
        }
      );
    });
  } catch (err) {
    console.log(err);
  }
};

exports.WalletHistory = async (req, res, next) => {
  try {
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      await authCheck.getUserId(
        req.headers.authorization,
        async (id, role, approve) => {
          if (req.params.type == 0) {
            var sqlDeposit = `SELECT NoTopup, amount, net, financing, statusPaid,datePaid FROM AmazieFintechP1.transactionTopUp where idUser=${id}`;
            connection.query(sqlDeposit, [], (err, result) => {
              if (err) {
                return res.status(200).send(resp(false, result, '', ''));
              } else {
                return res.status(200).send(resp(true, result, '', ''));
              }
            });
          } else if (req.params.type == 1) {
            var sqlDeposit = `SELECT No, amount, BankName,AccountName,AccountNumber ,
                                		CASE
                                            WHEN CEO = 1 AND CFO = 1
                                               THEN 'successful'
                                               ELSE 'pending'
                                       END AS Status,
                                       DateWithDraw
                                FROM AmazieFintechP1.transactionWithdraw where idUser = ${id};`;
            connection.query(sqlDeposit, [], (err, result) => {
              if (err) {
                return res.status(200).send(resp(false, result, '', ''));
              } else {
                return res.status(200).send(resp(true, result, '', ''));
              }
            });
          }
        }
      );
    });
  } catch (err) {
    console.log(err);
  }
};

exports.dashboard = async (req, res, next) => {
  try {
    let ResponseResult = [];
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      await authCheck.getUserId(
        req.headers.authorization,
        async (id, role, approve) => {
          var GetUser = GET_USER.code;
          await connection.query(GetUser, [id], async (err, result) => {
            if (err) {
              //   console.log(err);
              return res.status(200).send(resp(false, result, '', ''));
            } else {
              // ResponseResult.push(result)
              var AmountStock = GET_TRANSACTION_INVEST.code;
              await connection.query(AmountStock, [id], async (err, result) => {
                if (err) {
                  console.log(err);
                  return res.status(200).send(resp(false, result, '', ''));
                } else {
                  let AmountStockOfUser = 0;
                  for (let i = 0; i < result.length; i++) {
                    AmountStockOfUser = AmountStockOfUser + result[i].share;
                  }
                  await ResponseResult.push(AmountStockOfUser);
                  await ResponseResult.push(result);

                  var TransactionTopUp = GET_TRANSACTION_TOPUP.code;
                  await connection.query(
                    TransactionTopUp,
                    [id],
                    async (err, result) => {
                      if (err) {
                        console.log(err);
                        return res
                          .status(200)
                          .send(resp(false, result, '', ''));
                      } else {
                        ResponseResult.push(result);

                        let ResultResponse = {
                          AmountStockOfUser: ResponseResult[0],
                          TableTransactionInvestment: ResponseResult[1],
                          TableTransactionTopUp: ResponseResult[2]
                        };
                        return res
                          .status(200)
                          .send(resp(true, ResultResponse, '', ''));
                      }
                    }
                  );
                }
              });
            }
          });
        }
      );
    });
  } catch (err) {
    console.log(err);
  }
};

// exports.ApproveCommission = async (req, res) => {
//     try{

//     }
// }

exports.ApproveDivident = async (req, res, next) => {
  try {
    var { body } = req;
    let No = body.No;
    console.log('dividet : ', body);
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      // 4 CFO , 5 CEO
      authCheck.getUserId(
        req.headers.authorization,
        async (id, role, approve) => {
          if (role == 4) {
            var sql = APPROVE_DIVIDEND_CFO.code;
            connection.query(sql, [1, No], async (err, result) => {
              if (err) {
                return res.status(200).send(resp(false, result, '', ''));
              } else {
                return res
                  .status(200)
                  .send(
                    resp(true, result, '', 'อณุมัติการสั่งจ่ายปันผลเรียบร้อย')
                  );
              }
            });
          } else if (role == 5) {
            var sql = APPROVE_DIVIDEND_CEO.code;
            connection.query(sql, [1, No], async (err, result) => {
              if (err) {
                return res.status(200).send(resp(false, result, '', ''));
              } else {
                return res
                  .status(200)
                  .send(
                    resp(true, result, '', 'อณุมัติการสั่งจ่ายปันผลเรียบร้อย')
                  );
              }
            });
          } else if (role !== 5 || role !== 4) {
            console.log('สถานะไม่ถูกต้อง : ', role);
            return res
              .status(200)
              .send(resp(false, '', 'มึงกล้ามาก', 'สถานะไม่ถูกต้อง'));
          }
        }
      );
    });
  } catch (err) {
    console.log(err);
  }
};

function CheckSuccess(req, res, next, No, id) {
  req.getConnection((err, connection) => {
    if (err) return next(err);
    var sql = `SELECT * FROM AmazieFintechP1.transactionTopUp where NoTopup = '${No}' ;`;
    connection.query(sql, [], (err, result) => {
      if (err) {
        return res.status(200).send(resp(false, result, '', ''));
      } else {
        console.log('CheckSuccess success: ', result);
        if (
          result[0].CEO == 1 &&
          result[0].CFO == 1 &&
          result[0].financing == 'slip'
        ) {
          var amount = result[0].amount;
          var sql2 = `UPDATE  AmazieFintechP1.transactionTopUp SET statusPaid = 'successful' where NoTopup = '${No}' ;`;
          connection.query(sql2, [], (err, result2) => {
            if (err) {
              return res.status(200).send(resp(false, result2, '', ''));
            } else {
              var sql3 = `update AmazieFintechP1.user set user.balance = (user.balance + ${amount}) where user.id = ${id};`;
              connection.query(sql3, [], (err, result3) => {
                return res
                  .status(200)
                  .send(resp(true, result3, '', 'อณุมัติการเติมเงินเรียบร้อย'));
              });
            }
          });
        } else {
          return res
            .status(200)
            .send(resp(true, result, '', 'อณุมัติการเติมเงินเรียบร้อย'));
        }
      }
    });
  });
}

exports.ApproveTOPUP = async (req, res, next) => {
  try {
    var { body } = req;
    let No = body.No;
    console.log('ApproveTOPUP : ', body);
    await req.getConnection((err, connection) => {
      if (err) return next(err);
      // 4 CFO , 5 CEO
      authCheck.getUserId(
        req.headers.authorization,
        async (id, role, approve) => {
          var CheckULasted = `SELECT * FROM AmazieFintechP1.transactionTopUp where NoTopup = '${No}' ;`;
          connection.query(CheckULasted, [], (err, rr) => {
            if (err) {
              return res.status(200).send(resp(false, result, '', ''));
            } else {
              if (rr[0].CEO == 1 && rr[0].CFO == 1) {
                return res
                  .status(200)
                  .send(resp(true, rr, 'ห้ามปั๊มเงิน', 'สถานะไม่ถูกต้อง'));
              } else {
                if (role == 4) {
                  var sql = APPROVE_TOPUP_CFO.code;
                  connection.query(sql, [1, No], (err, result) => {
                    if (err) {
                      return res.status(200).send(resp(false, result, '', ''));
                    } else {
                      return CheckSuccess(req, res, next, No, id);
                    }
                  });
                } else if (role == 5) {
                  var sql = APPROVE_TOPUP_CEO.code;
                  connection.query(sql, [1, No], (err, result) => {
                    if (err) {
                      return res.status(200).send(resp(false, result, '', ''));
                    } else {
                      return CheckSuccess(req, res, next, No, id);
                    }
                  });
                } else if (role !== 5 || role !== 4) {
                  console.log('สถานะไม่ถูกต้อง : ', role);
                  return res
                    .status(200)
                    .send(resp(false, '', 'ห้ามปั๊มเงิน', 'สถานะไม่ถูกต้อง'));
                }
              }
            }
          });
        }
      );
    });
  } catch (err) {
    console.log(err);
  }
};

exports.ApproveWithDraw = async (req, res, next) => {
  try {
    var { body } = req;
    let No = body.No;
    console.log('dividet : ', body);
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      // 4 CFO , 5 CEO
      authCheck.getUserId(
        req.headers.authorization,
        async (id, role, approve) => {
          if (role == 4) {
            var sql = APPROVE_WITHDRAW_CFO.code;
            connection.query(sql, [1, No], async (err, result) => {
              if (err) {
                return res.status(200).send(resp(false, result, '', ''));
              } else {
                return res
                  .status(200)
                  .send(
                    resp(true, result, '', 'อณุมัติการสั่งจ่ายปันผลเรียบร้อย')
                  );
              }
            });
          } else if (role == 5) {
            var sql = APPROVE_WITHDRAW_CEO.code;
            connection.query(sql, [1, No], async (err, result) => {
              if (err) {
                return res.status(200).send(resp(false, result, '', ''));
              } else {
                return res
                  .status(200)
                  .send(
                    resp(true, result, '', 'อณุมัติการสั่งจ่ายปันผลเรียบร้อย')
                  );
              }
            });
          } else if (role !== 5 || role !== 4) {
            console.log('สถานะไม่ถูกต้อง : ', role);
            return res
              .status(200)
              .send(resp(false, '', 'มึงกล้ามาก', 'สถานะไม่ถูกต้อง'));
          }
        }
      );
    });
  } catch (err) {
    console.log(err);
  }
};

exports.ApproveCommission = async (req, res, next) => {
  try {
    var { body } = req;
    let No = body.No;
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      // 4 CFO , 5 CEO
      authCheck.getUserId(
        req.headers.authorization,
        async (id, role, approve) => {
          if (role == 4) {
            var sql = APPROVE_COMMISSION_CFO.code;
            connection.query(sql, [1, No], async (err, result) => {
              if (err) {
                return res.status(200).send(resp(false, result, '', ''));
              } else {
                return res
                  .status(200)
                  .send(
                    resp(
                      true,
                      result,
                      '',
                      'อณุมัติการสั่งจ่ายคอมมิสชั่นเรียบร้อย'
                    )
                  );
              }
            });
          } else if (role == 5) {
            var sql = APPROVE_COMMISSION_CEO.code;
            connection.query(sql, [1, No], async (err, result) => {
              if (err) {
                return res.status(200).send(resp(false, result, '', ''));
              } else {
                return res
                  .status(200)
                  .send(
                    resp(
                      true,
                      result,
                      '',
                      'อณุมัติการสั่งจ่ายคอมมิสชั่นเรียบร้อย'
                    )
                  );
              }
            });
          } else if (role !== 5 || role !== 4) {
            console.log('สถานะไม่ถูกต้อง : ', role);
            return res
              .status(200)
              .send(resp(false, '', 'สถานะไม่ถูกต้อง', 'สถานะไม่ถูกต้อง'));
          }
        }
      );
    });
  } catch (err) {
    console.log(err);
  }
};

exports.getPendingWithdraw = async (req, res, next) => {
  try {
    var { body } = req.body;
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      var sql = GET_TRANSACTION_WITHDRAW.code;
      connection.query(sql, [], (err, result) => {
        if (err) {
          return res.status(200).send(resp(false, result, '', ''));
        } else {
          return res.status(200).send(resp(true, result, '', ''));
        }
      });
    });
  } catch (err) {
    console.log(err);
  }
};
