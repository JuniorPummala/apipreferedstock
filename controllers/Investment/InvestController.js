const jwt = require('jsonwebtoken');
const expressJwt = require('express-jwt');
const config = require('./../../config');
const { validationResult } = require('express-validator/check');
const bcrypt = require('bcrypt');
const nodemailer = require('nodemailer');
var moment = require('moment-timezone');
var momentT = require('moment');
const sendgridTransport = require('nodemailer-sendgrid-transport');
const sgMail = require('@sendgrid/mail');
const {
  GET_PLAT,
  SELECT_PLAN,
  INSERT_TRANSACTION,
  CHECK_BALANC,
  QUERY_DASHBOARD_INVEST,
  QUERY_TRANSACTION,
  GET_PENDING_TRANSACTION
} = require('./InvestSQL.json');
const { resp } = require('../../helper/response');
const { connect } = require('pm2');
const connection = require('express-myconnection');
const authCheck = require('../../helper/getId');
require('dotenv').config();

const aws = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');

var omise = require('omise')({
  publicKey: process.env.PIBLIC_KEY_OMISE,
  secretKey: process.env.SECRET_KEY_OMISE
});

const transporter = nodemailer.createTransport(
  sendgridTransport({
    auth: {
      api_key: process.env.EMAIL_API_KEY
    }
  })
);

sgMail.setApiKey(process.env.EMAIL_API_KEY);

const spacesEndpoint = new aws.Endpoint(process.env.BUCKET_ENDPOINT);

const s3 = new aws.S3({
  endpoint: spacesEndpoint,
  accessKeyId: process.env.BUCKET_KEY,
  secretAccessKey: process.env.BUCKET_SECREET
});

exports.getPlaned = async (req, res, next) => {
  try {
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      var sql = GET_PLAT.code;
      await connection.query(sql, [], async (err, result) => {
        if (err) {
          return res.status(200).send(resp(false, result, '', ''));
        } else {
          return res.status(200).send(resp(true, result, '', ''));
        }
      });
    });
  } catch (err) {
    console.log(err);
  }
};

exports.SelectPlan = async (req, res, next) => {
  try {
    const { idPlan } = req.body;
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      var sql = SELECT_PLAN.code;
      await connection.query(sql, [idPlan], async (err, result) => {
        if (err) {
          return res.status(200).send(resp(false, result, '', ''));
        } else {
          return res.status(200).send(resp(true, result, '', ''));
        }
      });
    });
  } catch (err) {
    console.log(err);
  }
};

// switch (data.company_name) {
//   case 'Amazie Holding':
//     console.log('Holding : ');
//     break;
//   case 'Amazie Fintech':
//     console.log('Fintech : ');
//     break;
//   case 'Amazie Gold':
//     console.log('Gold : ');
//     break;
// }
exports.pendingTransaction = async (req, res) => {
  try {
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      await authCheck.getUserId(
        req.headers.authorization,
        async (id, role, approve) => {
          var sql = GET_PENDING_TRANSACTION.code;
          connection.query(sql, [id], async (err, result) => {
            if (err) {
              return res.status(200).send(resp(false, result, '', ''));
            } else {
              return res.status(200).send(resp(true, result, '', ''));
            }
          });
        }
      );
    });
  } catch (err) {
    console.log(err);
  }
};

exports.QueryInvestAmount = async (req, res, next) => {
  try {
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      await authCheck.getUserId(
        req.headers.authorization,
        async (id, role, approve) => {
          var sql = QUERY_DASHBOARD_INVEST.code;
          connection.query(sql, [id], async (err, result) => {
            if (err) {
              return res.status(200).send(resp(false, result, '', ''));
            } else {
              var resultQuery = [];

              let Holdinng = {
                company_name: null,
                InvestAmount: null,
                AmountOfShare: null,
                prefer: null,
                commond: null
              };
              let Fintech = {
                company_name: null,
                InvestAmount: null,
                AmountOfShare: null,
                prefer: null,
                commond: null
              };
              let Gold = {
                company_name: null,
                InvestAmount: null,
                AmountOfShare: null,
                prefer: null,
                commond: null
              };

              let world = null;
              let Mediheehub = null;
              let Glove = null;

              result.forEach((data) => {
                if (data.company_name == 'Amazie Holding') {
                  if (data.type == 1) {
                    Holdinng['company_name'] = 'Amazie Holding';
                    Holdinng['InvestAmount'] =
                      Holdinng['InvestAmount'] + data.InvestAmount;
                    Holdinng['AmountOfShare'] =
                      Holdinng['AmountOfShare'] + data.AmountOfShare;
                    Holdinng['prefer'] =
                      Holdinng['prefer'] + data.AmountOfShare;

                    //
                  } else {
                    Holdinng['company_name'] = 'Amazie Holding';
                    Holdinng['InvestAmount'] =
                      Holdinng['InvestAmount'] + data.InvestAmount;
                    Holdinng['AmountOfShare'] =
                      Holdinng['AmountOfShare'] + data.AmountOfShare;
                    Holdinng['commond'] =
                      Holdinng['commond'] + data.AmountOfShare;
                  }
                } else if (data.company_name == 'Amazie Fintech') {
                  if (data.type == 1) {
                    Fintech['company_name'] = 'Amazie Fintech';
                    Fintech['InvestAmount'] =
                      Fintech['InvestAmount'] + data.InvestAmount;
                    Fintech['AmountOfShare'] =
                      Fintech['AmountOfShare'] + data.AmountOfShare;
                    Fintech['prefer'] = Fintech['prefer'] + data.AmountOfShare;
                  } else {
                    Fintech['company_name'] = 'Amazie Fintech';
                    Fintech['InvestAmount'] =
                      Fintech['InvestAmount'] + data.InvestAmount;
                    Fintech['AmountOfShare'] =
                      Fintech['AmountOfShare'] + data.AmountOfShare;
                    Fintech['commond'] =
                      Fintech['commond'] + data.AmountOfShare;
                  }
                } else if (data.company_name == 'Amazie Gold') {
                  if (data.type == 1) {
                    Gold['company_name'] = 'Amazie Gold';
                    Gold['InvestAmount'] =
                      Gold['InvestAmount'] + data.InvestAmount;
                    Gold['AmountOfShare'] =
                      Gold['AmountOfShare'] + data.AmountOfShare;
                    Gold['prefer'] = Gold['prefer'] + data.AmountOfShare;
                  } else {
                    Gold['company_name'] = 'Amazie Gold';
                    Gold['InvestAmount'] =
                      Gold['InvestAmount'] + data.InvestAmount;
                    Gold['AmountOfShare'] =
                      Gold['AmountOfShare'] + data.AmountOfShare;
                    Gold['commond'] = Gold['commond'] + data.AmountOfShare;
                  }
                }
              });
              resultQuery.push(Holdinng, Fintech, Gold);
              console.log('resultQuery : ', resultQuery);
              return res.status(200).send(resp(true, resultQuery, '', ''));
            }
          });
        }
      );
    });
  } catch (err) {
    console.log(err);
  }
};

exports.QueryTransactionTable = async (req, res, next) => {
  try {
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      await authCheck.getUserId(
        req.headers.authorization,
        async (id, role, approve) => {
          var sql = QUERY_TRANSACTION.code;
          connection.query(sql, [id], async (err, result) => {
            if (err) {
              return res.status(200).send(resp(false, result, '', ''));
            } else {
              console.log('QueryTransactionTable', result);
              return res.status(200).send(resp(true, result, '', ''));
            }
          });
        }
      );
    });
  } catch (err) {
    console.log(err);
  }
};

exports.QueryDividendPending = async (req, res, next) => {
  try {
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      await authCheck.getUserId(
        req.headers.authorization,
        async (id, role, approve) => {
          var sql = QUERY_TRANSACTION.code;
        }
      );
    });
  } catch (err) {
    console.log(err);
  }
};
exports.BuyStock = async (req, res, next) => {
  try {
    const { contract, Plan, amount, share, dateStart, dateFinish, type } =
      req.body;
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      await authCheck.getUserId(
        req.headers.authorization,
        async (id, role, approve) => {
          var sqlBalance = CHECK_BALANC.code;
          connection.query(sqlBalance, [id], async (err, result) => {
            if (err) {
              return res.status(200).send(resp(false, result, '', ''));
            } else {
              if (result[0].balance <= 0) {
                console.log(result[0].balance);
                return res
                  .status(200)
                  .send(resp(false, result, '', 'จำนวนเงินในระบบไม่เพียงพอ'));
              } else {
                console.log('req.body : ', req.body);
                var sql = INSERT_TRANSACTION.code;
                connection.query(
                  sql,
                  [
                    contract,
                    Plan,
                    amount,
                    share,
                    id,
                    dateStart,
                    dateFinish,
                    type
                  ],
                  async (err, result) => {
                    if (err) {
                      return res
                        .status(200)
                        .send(resp(false, result, '', 'งง'));
                    } else {
                      return res
                        .status(200)
                        .send(resp(true, result, '', 'ผ่าน'));
                    }
                  }
                );
              }
            }
          });

          // await connection.query(
          //   sql,
          //   [contract, Plan, amount, share, id, dateStart, dateFinish],
          //   async (err, result) => {
          //     if (err) {
          //       return res.status(200).send(resp(false, result, '', ''));
          //     } else {
          //       return res.status(200).send(resp(true, result, '', ''));
          //     }
          //   }
          // );
        }
      );
    });
  } catch (err) {
    console.log(err);
  }
};
