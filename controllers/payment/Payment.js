const jwt = require('jsonwebtoken');
const expressJwt = require('express-jwt');
const config = require('./../../config');
const { validationResult } = require('express-validator/check');
const bcrypt = require('bcrypt');
const nodemailer = require('nodemailer');
var moment = require('moment-timezone');
var momentT = require('moment');
const sendgridTransport = require('nodemailer-sendgrid-transport');
const sgMail = require('@sendgrid/mail');
const {
  ADD_TRANSACTION_CREDIT,
  UPDATE_TRANSACTION_INTERNET_BANK,
  SEND_SLIP,
  WITHDRAW
} = require('./PaymentSQL.json');
const { resp } = require('../../helper/response');
const { connect } = require('pm2');
const connection = require('express-myconnection');
const authCheck = require('../../helper/getId');
require('dotenv').config();

const aws = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');

var omise = require('omise')({
  publicKey: process.env.PIBLIC_KEY_OMISE,
  secretKey: process.env.SECRET_KEY_OMISE
});

const transporter = nodemailer.createTransport(
  sendgridTransport({
    auth: {
      api_key: process.env.EMAIL_API_KEY
    }
  })
);

sgMail.setApiKey(process.env.EMAIL_API_KEY);

const spacesEndpoint = new aws.Endpoint(process.env.BUCKET_ENDPOINT);

const s3 = new aws.S3({
  endpoint: spacesEndpoint,
  accessKeyId: process.env.BUCKET_KEY,
  secretAccessKey: process.env.BUCKET_SECREET
});

const uploadSlip = multer({
  storage: multerS3({
    s3: s3,
    bucket: process.env.BUCKET,
    key: function (req, file, cb) {
      // console.log('file : ', file)
      cb(null, 'Slip' + '/' + new Date().getTime() + '_' + file.originalname);
    }
  })
}).single('slip');
// single('image');

exports.CheckOutInnteretBanking = async (req, res, next) => {
  // console.log(req.body);
  const { amount, source } = req.body;

  try {
    async function dd() {
      const charge = await omise.charges.create({
        amount: amount,
        currency: 'thb',
        return_uri: process.env.RETURN_URI,
        source: source
      });

      console.log('====================================');
      console.log(charge);
      console.log('====================================');

      if (charge) {
        return await req.getConnection(async (err, connection) => {
          if (err) return next(err);
          await authCheck.getUserId(
            req.headers.authorization,
            async (id, role, approve) => {
              var sql = ADD_TRANSACTION_CREDIT.code;
              var paidAT = moment(charge.created_at);
              var d = paidAT.tz('Asia/Bangkok').format('YYYY-MM-DD HH:mm:ss');
              console.log('charge : ', charge);

              await connection.query(
                sql,
                [
                  charge.id,
                  id,
                  '-',
                  charge.amount / 100,
                  0,
                  0,
                  '-',
                  '-',
                  '-',
                  charge.type,
                  '-',
                  0,
                  '-',
                  d,
                  d,
                  'ibanking',
                  1,
                  'pending'
                ],
                async (err, result) => {
                  if (err) {
                    console.log('err ', err);
                    return res.status(200).send(resp(false, charge, '', ''));
                  } else {
                    console.log('result ', result);
                    return res
                      .status(200)
                      .send(resp(true, charge, 'success wait for approve', ''));
                  }
                }
              );
            }
          );
        });
      }
    }
    dd();

    // omise.charges.create({
    //   'amount': amount,
    //   'currency': 'thb',
    //   'return_uri': 'http://www.example.com',
    //   'source': 'src_test_59wbyjr7jz44d8nzcd6'
    // }, async function(error, charge) {

    //   /* Response. */
    // });

    // if(charge){
    //     return  await req.getConnection(async (err, connection) => {
    //             if(err) return next(err);
    //             await authCheck.getUserId(req.headers.authorization, async (id, role, approve) => {

    //              var sql = ADD_TRANSACTION_CREDIT.code
    //              var paidAT =  moment(charge.created_at)
    //              var d = paidAT.tz('Asia/Bangkok').format('YYYY-MM-DD hh:mm:ss')
    //             console.log('charge : ' ,charge)
    //             await connection.query(sql, [charge.id,
    //                 id,
    //                 '-',
    //                 charge.amount/100,
    //                 '-',
    //                 '-',
    //                 '-',
    //                 '-',
    //                 '-',
    //                 charge.card.brand,
    //                 '-',
    //                 0,
    //                 '-',
    //                 d,
    //                 d,
    //                 charge.type,
    //                 1,
    //                 'pending'
    //             ], async(err, result) => {
    //                 if(err){
    //                      console.log('err ', err)
    //                     // return res.status(200).send(resp(false, result, "", ""));
    //                 }else{
    //                     console.log('result ', result)
    //                     // return res.status(200).send(resp(true, result, "success wait for approve", ""));
    //                 }
    //             })
    //          })
    //     })
    // }

    // console.log('start id : ', charge.id)
    // return res.status(200).send(resp(true, charge, "success wait for approve", ""));
  } catch (err) {
    console.log(err);
  }
};

exports.OmiseWebHook = async (req, res, next) => {
  const { data, key } = req.body;
  try {
    if (data.source) {
      console.log('data: ', data.status);
      var statusTransition = 0;
      if (data.status == 'successful') {
        statusTransition = 1;
      } else {
        statusTransition = 0;
      }
      console.log(' hook.id : ', data.id);
      console.log(' key : ', key);
      console.log(' data.source : ', data);
      var checkCreateAT = new Date();
      if (data.source == null) {
        checkCreateAT = data.card.created_at;
        console.log(' source nulll : ');
      } else {
        checkCreateAT = data.source.created_at;
        console.log('source  true ');
      }
      var paidAT = moment(checkCreateAT);
      var d = paidAT.tz('Asia/Bangkok').format('YYYY-MM-DD HH:mm:ss');
      const date_now = moment(new Date(), 'Asia/Bangkok').format(
        'YYYY-MM-DD HH:mm:ss'
      );
      if (key == 'charge.complete' || key == 'charge.create') {
        // if(data.status == 'successful' || data.status == 'failed'){
        return await req.getConnection(async (err, connection) => {
          if (err) return next(err);
          var sql = UPDATE_TRANSACTION_INTERNET_BANK.code;
          await connection.query(
            sql,
            [
              data.id,
              '-',
              data.amount / 100,
              data.net / 100,
              data.fee / 100,
              data.fee_vat / 100,
              '-',
              data.source.type,
              data.source.type,
              data.source.type,
              statusTransition,
              '-',
              d,
              date_now,
              'internet_banking',
              1,
              data.status,
              data.id
            ],
            async (err, result) => {
              if (err) {
                console.log(err);
                return res.status(200).send(resp(false, result, '', ''));
              } else {
                console.log('update success');
                return res.status(200).send(resp(true, result, '', ''));
              }
            }
          );
        });
        // }
      }
    }
  } catch (err) {
    console.log(err);
  }
  // next()
};

exports.SendSlipPayment = async (req, res, next) => {
  try {
    await uploadSlip(req, res, async function (err) {
      var { body } = req;
      let pathSlip = req.file.key;
      var amount = body.amount;
      var date = body.date;
      var time = body.time;
      var dateTime = `${body.date + ' ' + body.time}`;
      console.log('req.file.key : ', pathSlip);
      console.log('dateTime : ', dateTime);

      await req.getConnection(async (err, connection) => {
        if (err) return next(err);
        await authCheck.getUserId(
          req.headers.authorization,
          async (id, role, approve) => {
            await req.getConnection(async (err, connection) => {
              if (err) next(err);
              var sql = SEND_SLIP.code;
              await connection.query(
                sql,
                [
                  pathSlip,
                  id,
                  '-',
                  amount,
                  amount,
                  0,
                  0,
                  '-',
                  'slip',
                  '-',
                  '-',
                  0,
                  '-',
                  dateTime,
                  dateTime,
                  'transfer slip',
                  1,
                  'pending',
                  pathSlip
                ],
                (err, result) => {
                  if (err) {
                    return res.status(200).send(resp(false, err, null, null));
                  } else {
                    return res
                      .status(200)
                      .send(
                        resp(
                          true,
                          null,
                          'send slip to amazie successful',
                          'ส่งสลิปเรียบร้อย กรุณารอการยืนยันการเติมเงิน'
                        )
                      );
                  }
                }
              );
            });
          }
        );
      });
    });
  } catch (err) {
    console.log(err);
  }
};

exports.withDraw = async (req, res, next) => {
    const {
      amount,
      BankName,
      AccountName,
      AccountNumber,
      SwiftCode
    } = req.body;

  try {
    var { body } = req
    console.log('withdraw : ', body);
      var checkCreateAT = new Date();
      var paidAT = moment(checkCreateAT);
      var d = paidAT.tz('Asia/Bangkok').format('YYYY-MM-DD HH:mm:ss');
    await req.getConnection(async (err, connection) => {
         if (err) return next(err);
         authCheck.getUserId(req.headers.authorization, async (id, role, approve) => {
            var sql = WITHDRAW.code
            connection.query(
              sql,
              [
                id,
                amount,
                BankName,
                AccountName,
                AccountNumber,
                SwiftCode,
                d
              ],
              (err, result) => {
                if (err) {
                return res.status(200).send(resp(false, result, '', ''));
                }else{
                return res.status(200).send(resp(true, result, '', 'ส่งคำสั่งถอนเรียบร้อย'));
                }
              }
            );
        })
    })
  } catch (err) {
    console.log(err);
  }
};

exports.CheckOutCreditCard = async (req, res, next) => {
  const { amount, token } = req.body;
  try {
    // let a = parseFloat(amount*100)

    // const customer  = await omise.customers.create({
    //     email,
    //     description: name,
    //     card: token
    // })

    const charge = await omise.charges.create({
      amount: amount,
      currency: 'thb',
      card: token
      // customer: customer.id
    });

    if (charge) {
      return await req.getConnection(async (err, connection) => {
        if (err) return next(err);
        await authCheck.getUserId(
          req.headers.authorization,
          async (id, role, approve) => {
            var sql = ADD_TRANSACTION_CREDIT.code;
            var paidAT = moment(charge.paid_at);
            var d = paidAT.tz('Asia/Bangkok').format('YYYY-MM-DD hh:mm:ss');
            //  var paidAT = charge.paid_at.replace('Z', '').replace('T', ' ')
            const datePaid = moment(paidAT)
              .add(7, 'hours')
              .format('YYYY-MM-DD hh:mm A');
            const date_now = moment(paidAT)
              .add(7, 'hours')
              .format('YYYY-MM-DD hh:mm:ss');
            console.log('paidAT', d);
            // console.log(charge)
            await connection.query(
              sql,
              [
                charge.id,
                id,
                charge.card.name,
                charge.amount / 100,
                charge.net / 100,
                charge.fee / 100,
                charge.fee_vat / 100,
                charge.card.last_digits,
                charge.card.financing,
                charge.card.brand,
                '-',
                1,
                '-',
                d,
                d,
                charge.card.object,
                1,
                charge.status
              ],
              async (err, result) => {
                if (err) {
                  return res.status(200).send(resp(false, result, '', ''));
                } else {
                  return res
                    .status(200)
                    .send(resp(true, result, 'success wait for approve', ''));
                }
              }
            );
          }
        );
      });
    }
    console.log('charge : ', charge.status);
  } catch (err) {
    console.log(err);
  }
  // next()
};

exports.withDrawal = async (req, res, next) => {
  try {
    const {} = req.body;
  } catch (err) {
    console.log(err);
  }
  next();
};
