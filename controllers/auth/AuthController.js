const jwt = require('jsonwebtoken');
const expressJwt = require('express-jwt');
const config = require('./../../config');
const { validationResult } = require('express-validator/check');
const bcrypt = require('bcrypt');
const nodemailer = require('nodemailer');
var moment = require('moment-timezone');
const sendgridTransport = require('nodemailer-sendgrid-transport');
const sgMail = require('@sendgrid/mail');
const {
  FINE_BY_EMAIL,
  REGISTER_USER,
  UPDATE_VERIFY_BYEMAIL,
  UPDATE_VERIFY_SUCCESS,
  FINE_BY_ID,
  FINE_INDEX_KYC_BY_ID,
  RESET_PASSWORD,
  CHANGE_VERIFY_CODE,
  CHANGE_VERIFY_CODE_FORGET,
  CHECK_REFID,
  GET_COUNTYR,
  UPDATE_INFORMATION,
  APPROVE_KYC
} = require('./AuthSQL.json');
const { resp } = require('../../helper/response');
const { connect } = require('pm2');
const connection = require('express-myconnection');
const authCheck = require('../../helper/getId');
require('dotenv').config();

const aws = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');

const transporter = nodemailer.createTransport(
  sendgridTransport({
    auth: {
      api_key: process.env.EMAIL_API_KEY
    }
  })
);
sgMail.setApiKey(process.env.EMAIL_API_KEY);

const spacesEndpoint = new aws.Endpoint(process.env.BUCKET_ENDPOINT);

const s3 = new aws.S3({
  endpoint: spacesEndpoint,
  accessKeyId: process.env.BUCKET_KEY,
  secretAccessKey: process.env.BUCKET_SECREET
});

const uploadkyc = multer({
  storage: multerS3({
    s3: s3,
    bucket: process.env.BUCKET,
    key: function (req, file, cb) {
      // console.log('file : ', file)
      cb(null, 'kyc' + '/' + new Date().getTime() + '_' + file.originalname);
    }
  })
}).array('image', 4);
// single('image');

exports.upload = async (req, res, next) => {
  // console.log('req Img : ', req.body)
  await uploadkyc(req, res, async function (err) {
    if (err) {
      res.status(400).json({
        success: 'error',
        message: err.message
      });
      if (err) return next(err);
    } else {
      // let path = req.file.key;
      // let file = req.file
      let fileArray = req.files,
        fileLocation;
      const images = [];
      for (let i = 0; i < fileArray.length; i++) {
        fileLocation = fileArray[i];
        console.log('filenm', fileLocation);
        images.push(fileLocation);
      }
      console.log('name : ', req.body.name);
      res.status(200).json({
        success: 'success',
        message: 'Image Uploaded Successfully !'
        // path: path
      });
    }
  });
};

exports.getImageKYC = (req, res, next) => {
  console.log('sasdc : ', req.params.path + '/' + req.params.path2);
  var getParams = {
    Bucket: process.env.BUCKET,
    Key: req.params.path + '/' + req.params.path2
  };
  s3.getObject(getParams, async function (err, data) {
    if (err) {
      if (err) return next(err);
    } else {
      // res.status(200).send(resp(true, data.body, null, null));
      res.writeHead(200, {
        'Content-Type': 'image/png'
      });
      res.write(data.Body, 'binary');
      res.end(null, 'binary');
    }
  });
};

exports.getImageFaceIMG = (req, res, next) => {
  var { body } = req;
  authCheck.getUserId(req.headers.authorization, async (id, role, approve) => {
    await req.getConnection(async (err, connection) => {
      if (err) next(err);
      var sql = FINE_INDEX_KYC_BY_ID.code;
      await connection.query(sql, [id], async (err, result) => {
        console.log(result);

        var getParams = {
          Bucket: process.env.BUCKET,
          Key: result[0].FaceIMG
        };
        var getParams2 = {
          Bucket: process.env.BUCKET,
          Key: result.BookbankIMG
        };
        var getParams3 = {
          Bucket: process.env.BUCKET,
          Key: result.FrontPassportIMG
        };
        var getParams4 = {
          Bucket: process.env.BUCKET,
          Key: result.BackPassportIMG
        };

        s3.getObject(getParams, async function (err, data) {
          if (err) {
            if (err) return next(err);
          } else {
            // res.status(200).send(resp(true, data.body, null, null));
            res.writeHead(200, {
              'Content-Type': 'image/png'
            });
            res.write(data.Body, 'binary');
            res.end(null, 'binary');
          }
        });

        // if (err) {
        //   res.status(200).send(resp(false, result[0], null, null));
        // } else if (result.length > 0) {
        //   res.status(200).send(resp(true, result[0], null, null));
        // } else {
        //   res.status(200).send(resp(false, result[0], null, null));
        // }
      });
    });
  });
};
exports.getImageBookbankIMG = (req, res, next) => {
  var { body } = req;
  authCheck.getUserId(req.headers.authorization, async (id, role, approve) => {
    await req.getConnection(async (err, connection) => {
      if (err) next(err);
      var sql = FINE_INDEX_KYC_BY_ID.code;
      await connection.query(sql, [id], async (err, result) => {
        console.log(result);

        var getParams = {
          Bucket: process.env.BUCKET,
          Key: result[0].BookbankIMG
        };

        s3.getObject(getParams, async function (err, data) {
          if (err) {
            if (err) return next(err);
          } else {
            // res.status(200).send(resp(true, data.body, null, null));
            res.writeHead(200, {
              'Content-Type': 'image/png'
            });
            res.write(data.Body, 'binary');
            res.end(null, 'binary');
          }
        });

        // if (err) {
        //   res.status(200).send(resp(false, result[0], null, null));
        // } else if (result.length > 0) {
        //   res.status(200).send(resp(true, result[0], null, null));
        // } else {
        //   res.status(200).send(resp(false, result[0], null, null));
        // }
      });
    });
  });
};
exports.getImageFrontPassportIMG = (req, res, next) => {
  var { body } = req;
  authCheck.getUserId(req.headers.authorization, async (id, role, approve) => {
    await req.getConnection(async (err, connection) => {
      if (err) next(err);
      var sql = FINE_INDEX_KYC_BY_ID.code;
      await connection.query(sql, [id], async (err, result) => {
        // console.log(result);

        var getParams = {
          Bucket: process.env.BUCKET,
          Key: result[0].FrontPassportIMG
        };

        s3.getObject(getParams, async function (err, data) {
          if (err) {
            if (err) return next(err);
          } else {
            // res.status(200).send(resp(true, data.body, null, null));
            res.writeHead(200, {
              'Content-Type': 'image/png'
            });
            res.write(data.Body, 'binary');
            res.end(null, 'binary');
          }
        });

        // if (err) {
        //   res.status(200).send(resp(false, result[0], null, null));
        // } else if (result.length > 0) {
        //   res.status(200).send(resp(true, result[0], null, null));
        // } else {
        //   res.status(200).send(resp(false, result[0], null, null));
        // }
      });
    });
  });
};
exports.getImageBackPassportIMG = (req, res, next) => {
  var { body } = req;
  authCheck.getUserId(req.headers.authorization, async (id, role, approve) => {
    await req.getConnection(async (err, connection) => {
      if (err) next(err);
      var sql = FINE_INDEX_KYC_BY_ID.code;
      await connection.query(sql, [id], async (err, result) => {
        // console.log(result);

        var getParams = {
          Bucket: process.env.BUCKET,
          Key: result[0].BackPassportIMG
        };

        s3.getObject(getParams, async function (err, data) {
          if (err) {
            if (err) return next(err);
          } else {
            // res.status(200).send(resp(true, data.body, null, null));
            res.writeHead(200, {
              'Content-Type': 'image/png'
            });
            res.write(data.Body, 'binary');
            res.end(null, 'binary');
          }
        });

        // if (err) {
        //   res.status(200).send(resp(false, result[0], null, null));
        // } else if (result.length > 0) {
        //   res.status(200).send(resp(true, result[0], null, null));
        // } else {
        //   res.status(200).send(resp(false, result[0], null, null));
        // }
      });
    });
  });
};

async function approvekyc(email) {
  const msg = {
    to: email,
    from: 'praprute@amaziefintech.com',
    subject: 'Approve KYC',
    headings: 'Welcome To Amazie Fintech. The Future of Financial',
    html: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html data-editor-version="2" class="sg-campaigns" xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
      <!--[if !mso]><!-->
      <meta http-equiv="X-UA-Compatible" content="IE=Edge">
      <!--<![endif]-->
      <!--[if (gte mso 9)|(IE)]>
      <xml>
        <o:OfficeDocumentSettings>
          <o:AllowPNG/>
          <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
      </xml>
      <![endif]-->
      <!--[if (gte mso 9)|(IE)]>
  <style type="text/css">
    body {width: 600px;margin: 0 auto;}
    table {border-collapse: collapse;}
    table, td {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}
    img {-ms-interpolation-mode: bicubic;}
  </style>
<![endif]-->
      <style type="text/css">
    body, p, div {
      font-family: arial,helvetica,sans-serif;
      font-size: 14px;
    }
    body {
      color: #000000;
    }
    body a {
      color: #1188E6;
      text-decoration: none;
    }
    p { margin: 0; padding: 0; }
    table.wrapper {
      width:100% !important;
      table-layout: fixed;
      -webkit-font-smoothing: antialiased;
      -webkit-text-size-adjust: 100%;
      -moz-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
    }
    img.max-width {
      max-width: 100% !important;
    }
    .column.of-2 {
      width: 50%;
    }
    .column.of-3 {
      width: 33.333%;
    }
    .column.of-4 {
      width: 25%;
    }
    ul ul ul ul  {
      list-style-type: disc !important;
    }
    ol ol {
      list-style-type: lower-roman !important;
    }
    ol ol ol {
      list-style-type: lower-latin !important;
    }
    ol ol ol ol {
      list-style-type: decimal !important;
    }
    @media screen and (max-width:480px) {
      .preheader .rightColumnContent,
      .footer .rightColumnContent {
        text-align: left !important;
      }
      .preheader .rightColumnContent div,
      .preheader .rightColumnContent span,
      .footer .rightColumnContent div,
      .footer .rightColumnContent span {
        text-align: left !important;
      }
      .preheader .rightColumnContent,
      .preheader .leftColumnContent {
        font-size: 80% !important;
        padding: 5px 0;
      }
      table.wrapper-mobile {
        width: 100% !important;
        table-layout: fixed;
      }
      img.max-width {
        height: auto !important;
        max-width: 100% !important;
      }
      a.bulletproof-button {
        display: block !important;
        width: auto !important;
        font-size: 80%;
        padding-left: 0 !important;
        padding-right: 0 !important;
      }
      .columns {
        width: 100% !important;
      }
      .column {
        display: block !important;
        width: 100% !important;
        padding-left: 0 !important;
        padding-right: 0 !important;
        margin-left: 0 !important;
        margin-right: 0 !important;
      }
      .social-icon-column {
        display: inline-block !important;
      }
    }
  </style>
      <!--user entered Head Start--><!--End Head user entered-->
    </head>
    <body>
      <center class="wrapper" data-link-color="#1188E6" data-body-style="font-size:14px; font-family:arial,helvetica,sans-serif; color:#000000; background-color:#d1d1d1;">
        <div class="webkit">
          <table cellpadding="0" cellspacing="0" border="0" width="100%" class="wrapper" bgcolor="#d1d1d1">
            <tr>
              <td valign="top" bgcolor="#d1d1d1" width="100%">
                <table width="100%" role="content-container" class="outer" align="center" cellpadding="0" cellspacing="0" border="0">
                  <tr>
                    <td width="100%">
                      <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                          <td>
                            <!--[if mso]>
    <center>
    <table><tr><td width="600">
  <![endif]-->
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="width:100%; max-width:600px;" align="center">
                                      <tr>
                                        <td role="modules-container" style="padding:20px 20px 20px 20px; color:#000000; text-align:left;" bgcolor="#FFFFFF" width="100%" align="left"><table class="module preheader preheader-hide" role="module" data-type="preheader" border="0" cellpadding="0" cellspacing="0" width="100%" style="display: none !important; mso-hide: all; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;">
    <tr>
      <td role="module-content">
        <p></p>
      </td>
    </tr>
  </table><table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns" style="padding:0px 0px 0px 0px;" bgcolor="#FFFFFF" data-distribution="1">
    <tbody>
      <tr role="module-content">
        <td height="100%" valign="top"><table width="540" style="width:540px; border-spacing:0; border-collapse:collapse; margin:0px 10px 0px 10px;" cellpadding="0" cellspacing="0" align="left" border="0" bgcolor="" class="column column-0">
      <tbody>
        <tr>
          <td style="padding:0px;margin:0px;border-spacing:0;"><table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="07eced87-0768-47a8-9118-9710187e41f1">
    <tbody>
      <tr>
        <td style="font-size:6px; line-height:10px; padding:0px 0px 0px 0px;" valign="top" align="center">
          <img class="max-width" border="0" style="display:block; color:#000000; text-decoration:none; font-family:Helvetica, arial, sans-serif; font-size:16px; max-width:25% !important; width:25%; height:auto !important;" width="135" alt="" data-proportionally-constrained="true" data-responsive="true" src="http://cdn.mcauto-images-production.sendgrid.net/82176fcceb4c4c79/bcb93716-d6b8-45a1-a378-1ca0a559df5f/906x993.png">
        </td>
      </tr>
    </tbody>
  </table></td>
        </tr>
      </tbody>
    </table></td>
      </tr>
    </tbody>
  </table><table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="6375e32b-d6a3-441c-8be8-fb132ce2eaf8" data-mc-module-version="2019-10-22">
    <tbody>
      <tr>
        <td style="padding:18px 0px 18px 0px; line-height:22px; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content"><div><div style="font-family: inherit; text-align: inherit"><span style="font-family: helvetica, sans-serif"><strong>Greetings ${email},</strong></span></div><div></div></div></td>
      </tr>
    </tbody>
  </table><table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="30d83209-c377-4e52-a5f5-f29a83d9c4fc" data-mc-module-version="2019-10-22">
    <tbody>
      <tr>
        <td style="padding:10px 0px 5px 0px; line-height:NaNpx; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content"><div><div style="font-family: inherit; text-align: inherit"><span style="font-family: helvetica, sans-serif; font-size: 14px">Congratulations!</span></div><div></div></div></td>
      </tr>
    </tbody>
  </table><table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="91e3fb7b-e180-4516-ae8e-1ded1b5e1887" data-mc-module-version="2019-10-22">
    <tbody>
      <tr>
        <td style="padding:18px 0px 18px 0px; line-height:22px; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content"><div><div style="font-family: inherit; text-align: inherit"><span style="font-family: helvetica, sans-serif">Your documents has been reviewed and accepted. From now on, you can login into our website at </span><a href="https://www.amaziefintech.com/"><span style="font-family: helvetica, sans-serif">https://www.amaziefintech.com/</span></a><span style="font-family: helvetica, sans-serif">&nbsp;</span></div><div></div></div></td>
      </tr>
    </tbody>
  </table><table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="385f83e2-e307-4ddf-a151-eb381845e931" data-mc-module-version="2019-10-22">
    <tbody>
      <tr>
        <td style="padding:18px 0px 0px 0px; line-height:22px; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content"><div><div style="font-family: inherit; text-align: inherit">NEED HELP? CONTACT TO <a href="mailto:contact@amaziefintech.com?subject=&amp;body=">contact@amaziefintech.com</a></div><div></div></div></td>
      </tr>
    </tbody>
  </table><table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="2fc9d5fa-16bd-4d9d-9371-d0694782ea1e" data-mc-module-version="2019-10-22">
    <tbody>
      <tr>
        <td style="padding:0px 0px 18px 0px; line-height:22px; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content"><div><div style="font-family: inherit; text-align: inherit">Thanks and regards,</div><div></div></div></td>
      </tr>
    </tbody>
  </table><table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="3ed2454d-6bde-4f28-9d43-6a55edf8ed7b" data-mc-module-version="2019-10-22">
    <tbody>
      <tr>
        <td style="padding:0px 0px 18px 0px; line-height:22px; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content"><div><div style="font-family: inherit; text-align: inherit"><span style="font-family: helvetica, sans-serif"><strong>Amazie Fintech Team.</strong></span></div><div></div></div></td>
      </tr>
    </tbody>
  </table><table class="module" role="module" data-type="divider" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="c83c3815-4691-4ea7-ad24-1eaf8d33658e">
    <tbody>
      <tr>
        <td style="padding:0px 0px 0px 0px;" role="module-content" height="100%" valign="top" bgcolor="">
          <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" height="1px" style="line-height:1px; font-size:1px;">
            <tbody>
              <tr>
                <td style="padding:0px 0px 1px 0px;" bgcolor="#cecece"></td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table><table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="2ebc3048-bd42-4eb2-bad4-a0af0b262255" data-mc-module-version="2019-10-22">
    <tbody>
      <tr>
        <td style="padding:0px 0px 18px 0px; line-height:22px; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content"><div><div style="font-family: inherit; text-align: center"><span style="color: #909298; font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 10px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial; float: none; display: inline">© 2021 Amazie Fintech, All Rights Reserved.&nbsp;</span></div><div></div></div></td>
      </tr>
    </tbody>
  </table></td>
                                      </tr>
                                    </table>
                                    <!--[if mso]>
                                  </td>
                                </tr>
                              </table>
                            </center>
                            <![endif]-->
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </div>
      </center>
    </body>
  </html>`
  };
  sgMail
    .send(msg)
    .then(() => {
      console.log('Email KYC sent');
    })
    .catch((error) => {
      console.error(error);
    });
}

async function SendEmailVerify(email, val) {
  const msg = {
    to: email,
    from: 'praprute@amaziefintech.com',
    subject: 'Signup succeeded! :)',
    headings: 'Welcome To Amazie Fintech. The Future of Financial',
    html: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html data-editor-version="2" class="sg-campaigns" xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
      <!--[if !mso]><!-->
      <meta http-equiv="X-UA-Compatible" content="IE=Edge">
      <!--<![endif]-->
      <!--[if (gte mso 9)|(IE)]>
      <xml>
        <o:OfficeDocumentSettings>
          <o:AllowPNG/>
          <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
      </xml>
      <![endif]-->
      <!--[if (gte mso 9)|(IE)]>
  <style type="text/css">
    body {width: 650px;margin: 0 auto;}
    table {border-collapse: collapse;}
    table, td {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}
    img {-ms-interpolation-mode: bicubic;}
  </style>
<![endif]-->
      <style type="text/css">
    body, p, div {
      font-family: inherit;
      font-size: 14px;
    }
    body {
      color: #000000;
    }
    body a {
      color: #1188E6;
      text-decoration: none;
    }
    p { margin: 0; padding: 0; }
    table.wrapper {
      width:100% !important;
      table-layout: fixed;
      -webkit-font-smoothing: antialiased;
      -webkit-text-size-adjust: 100%;
      -moz-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
    }
    img.max-width {
      max-width: 100% !important;
    }
    .column.of-2 {
      width: 50%;
    }
    .column.of-3 {
      width: 33.333%;
    }
    .column.of-4 {
      width: 25%;
    }
    ul ul ul ul  {
      list-style-type: disc !important;
    }
    ol ol {
      list-style-type: lower-roman !important;
    }
    ol ol ol {
      list-style-type: lower-latin !important;
    }
    ol ol ol ol {
      list-style-type: decimal !important;
    }
    @media screen and (max-width:480px) {
      .preheader .rightColumnContent,
      .footer .rightColumnContent {
        text-align: left !important;
      }
      .preheader .rightColumnContent div,
      .preheader .rightColumnContent span,
      .footer .rightColumnContent div,
      .footer .rightColumnContent span {
        text-align: left !important;
      }
      .preheader .rightColumnContent,
      .preheader .leftColumnContent {
        font-size: 80% !important;
        padding: 5px 0;
      }
      table.wrapper-mobile {
        width: 100% !important;
        table-layout: fixed;
      }
      img.max-width {
        height: auto !important;
        max-width: 100% !important;
      }
      a.bulletproof-button {
        display: block !important;
        width: auto !important;
        font-size: 80%;
        padding-left: 0 !important;
        padding-right: 0 !important;
      }
      .columns {
        width: 100% !important;
      }
      .column {
        display: block !important;
        width: 100% !important;
        padding-left: 0 !important;
        padding-right: 0 !important;
        margin-left: 0 !important;
        margin-right: 0 !important;
      }
      .social-icon-column {
        display: inline-block !important;
      }
    }
  </style>
      <!--user entered Head Start--><link href="https://fonts.googleapis.com/css?family=Chivo&display=swap" rel="stylesheet"><style>
body {font-family: 'Chivo', sans-serif;}
</style><!--End Head user entered-->
    </head>
    <body>
      <center class="wrapper" data-link-color="#1188E6" data-body-style="font-size:14px; font-family:inherit; color:#000000; background-color:#E4E7E9;">
        <div class="webkit">
          <table cellpadding="0" cellspacing="0" border="0" width="100%" class="wrapper" bgcolor="#E4E7E9">
            <tr>
              <td valign="top" bgcolor="#E4E7E9" width="100%">
                <table width="100%" role="content-container" class="outer" align="center" cellpadding="0" cellspacing="0" border="0">
                  <tr>
                    <td width="100%">
                      <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                          <td>
                            <!--[if mso]>
    <center>
    <table><tr><td width="650">
  <![endif]-->
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="width:100%; max-width:650px;" align="center">
                                      <tr>
                                        <td role="modules-container" style="padding:0px 0px 0px 0px; color:#000000; text-align:left;" bgcolor="#ffffff" width="100%" align="left"><table class="module preheader preheader-hide" role="module" data-type="preheader" border="0" cellpadding="0" cellspacing="0" width="100%" style="display: none !important; mso-hide: all; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;">
    <tr>
      <td role="module-content">
        <p></p>
      </td>
    </tr>
  </table><table class="module" role="module" data-type="spacer" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="09414a0e-e7c9-462e-8748-d2deeb39587e">
    <tbody>
      <tr>
        <td style="padding:0px 0px 50px 0px;" role="module-content" bgcolor="">
        </td>
      </tr>
    </tbody>
  </table><table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="baf2d048-8ecb-44b4-a2d7-7949a7e77cb4">
    <tbody>
      <tr>
        <td style="font-size:6px; line-height:10px; padding:0px 0px 0px 0px;" valign="top" align="center">
          <img class="max-width" border="0" style="display:block; color:#000000; text-decoration:none; font-family:Helvetica, arial, sans-serif; font-size:16px; max-width:20% !important; width:20%; height:auto !important;" width="130" alt="" data-proportionally-constrained="true" data-responsive="true" src="http://cdn.mcauto-images-production.sendgrid.net/82176fcceb4c4c79/2d300cca-ce70-487a-a7ec-cb1b963f6cf7/906x993.png">
        </td>
      </tr>
    </tbody>
  </table><table class="module" role="module" data-type="spacer" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="ac68226b-ca94-4896-844f-c9bee13155b6">
    <tbody>
      <tr>
        <td style="padding:0px 0px 10px 0px;" role="module-content" bgcolor="">
        </td>
      </tr>
    </tbody>
  </table><table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="ceaf376c-efe4-4985-8599-58d6aac99484.1" data-mc-module-version="2019-10-22">
    <tbody>
      <tr>
        <td style="padding:18px 0px 18px 0px; line-height:20px; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content"><div><div style="font-family: inherit; text-align: center"><span style="font-size: 24px; font-family: helvetica, sans-serif">Verify your email address</span></div><div></div></div></td>
      </tr>
    </tbody>
  </table><table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="70f1e187-58fb-4516-b6bf-328cccc58fe8" data-mc-module-version="2019-10-22">
    <tbody>
      <tr>
        <td style="padding:0px 0px 0px 0px; line-height:20px; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content"><div><div style="font-family: inherit; text-align: center"><span style="color: #909091">Please confirm that you want to use this as your Amazie Fintech account</span></div>
<div style="font-family: inherit; text-align: center"><span style="color: #909091">email address. Once it's done you will be able to start investing!&nbsp;</span></div><div></div></div></td>
      </tr>
    </tbody>
  </table><table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="ceaf376c-efe4-4985-8599-58d6aac99484" data-mc-module-version="2019-10-22">
    <tbody>
      <tr>
        <td style="padding:18px 0px 18px 0px; line-height:20px; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content"><div><div style="font-family: inherit; text-align: center"><span style="font-family: helvetica, sans-serif; font-size: 18px">The verification code is : ${val}</span></div><div></div></div></td>
      </tr>
    </tbody>
  </table><table border="0" cellpadding="0" cellspacing="0" class="module" data-role="module-button" data-type="button" role="module" style="table-layout:fixed;" width="100%" data-muid="aa3d9a32-5061-4336-8560-2fb57e0cb416">
      <tbody>
        <tr>
          <td align="center" bgcolor="" class="outer-td" style="padding:0px 0px 0px 0px;">
            <table border="0" cellpadding="0" cellspacing="0" class="wrapper-mobile" style="text-align:center;">
              <tbody>
                <tr>
                <td align="center" bgcolor="#3ea314" class="inner-td" style="border-radius:6px; font-size:16px; text-align:center; background-color:inherit;">
                  <a href="" style="background-color:#3ea314; border:1px solid #3ea314; border-color:#3ea314; border-radius:3px; border-width:1px; color:#ffffff; display:inline-block; font-size:14px; font-weight:normal; letter-spacing:0px; line-height:normal; padding:12px 18px 12px 18px; text-align:center; text-decoration:none; border-style:solid;" target="_blank">Verify my email</a>
                </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table><table class="module" role="module" data-type="spacer" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="7f284da2-9c52-4f0f-bf1e-a52ca8069199">
    <tbody>
      <tr>
        <td style="padding:0px 0px 10px 0px;" role="module-content" bgcolor="">
        </td>
      </tr>
    </tbody>
  </table><table class="module" role="module" data-type="divider" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="691bee52-2501-4b8c-af88-19f640c61ed3">
    <tbody>
      <tr>
        <td style="padding:0px 40px 0px 40px;" role="module-content" height="100%" valign="top" bgcolor="">
          <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" height="1px" style="line-height:1px; font-size:1px;">
            <tbody>
              <tr>
                <td style="padding:0px 0px 1px 0px;" bgcolor="#bbbbbc"></td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table><table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="05324008-c53c-4fcc-b99a-9c59ca9d0d1c" data-mc-module-version="2019-10-22">
    <tbody>
      <tr>
        <td style="padding:0px 0px 0px 0px; line-height:22px; text-align:inherit;" height="100%" valign="top" bgcolor="" role="module-content"><div><div style="font-family: inherit; text-align: center"><span style="color: #909298; font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial; float: none; display: inline; font-size: 10px">© 2021 Amazie Fintech, All Rights Reserved.&nbsp;</span></div><div></div></div></td>
      </tr>
    </tbody>
  </table><table class="module" role="module" data-type="spacer" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-muid="d9c62ac8-fdfb-44c0-aeca-d6b8488c6155">
    <tbody>
      <tr>
        <td style="padding:0px 0px 50px 0px;" role="module-content" bgcolor="">
        </td>
      </tr>
    </tbody>
  </table></td>
                                      </tr>
                                    </table>
                                    <!--[if mso]>
                                  </td>
                                </tr>
                              </table>
                            </center>
                            <![endif]-->
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </div>
      </center>
    </body>
  </html>`
  };
  sgMail
    .send(msg)
    .then(() => {
      console.log('Email sent');
    })
    .catch((error) => {
      console.error(error);
    });
}

async function CheckRefId(connection, res, refId) {
  try {
    var sql = CHECK_REFID.code;
    await connection.query(sql, [refId], async (err, result) => {
      console.log(' result : ', result);
      if (result[0]) {
        return 'true';
      } else {
        return 'false';
      }
    });
  } catch (err) {
    res.status(500).send({
      success: 'ERROR',
      message: err.message
    });
  }
}

exports.FetchCountry = async (req, res, next) => {
  try {
    var { body } = req;
    await req.getConnection(async (err, connection) => {
      if (err) next(err);
      var sql = GET_COUNTYR.code;
      await connection.query(sql, null, async (err, result) => {
        if (err) next(err);
        res.status(200).send(resp(true, result, null, null));
      });
    });
  } catch (err) {
    res.status(500).send({
      success: 'ERROR',
      message: err.message
    });
  }
};

exports.FecthIndexKYC = async (req, res, next) => {
  try {
    var { body } = req;
    let idUser = body.idUser;
    authCheck.getUserId(
      req.headers.authorization,
      async (id, role, approve) => {
        await req.getConnection(async (err, connection) => {
          if (err) next(err);
          var sql = FINE_INDEX_KYC_BY_ID.code;
          await connection.query(sql, [idUser], async (err, result) => {
            if (err) {
              res.status(200).send(resp(false, result[0], null, null));
            } else if (result.length > 0) {
              res.status(200).send(resp(true, result[0], null, null));
            } else {
              res.status(200).send(resp(false, result[0], null, null));
            }
          });
        });
      }
    );
  } catch (err) {
    res.status(500).send({
      success: 'ERROR',
      message: err.message
    });
  }
};

exports.updateKYC = async (req, res, next) => {
  try {
    await uploadkyc(req, res, async function (err) {
      var { body } = req;
      let email = body.email;
      let password = body.password;
      let firstname = body.firstname;
      let lastname = body.lastname;
      let birth = body.birth;
      let gender = body.gender;
      let phone = body.phone;
      let country = body.country;
      let city = body.city;
      let poscode = body.poscode;
      let address1 = body.address1;
      let address2 = body.address2;
      let national = body.national;
      let passport = body.passport;
      let employeeStatus = body.employeeStatus;
      let employeeAddress = body.employeeAddress;
      let role = body.role;
      let refCode = body.refCode;
      if (err) {
        res.status(400).json({
          success: 'error',
          message: err.message
        });
        if (err) return next(err);
      } else {
        let fileArray = req.files,
          fileLocation;
        const images = [];
        for (let i = 0; i < fileArray.length; i++) {
          fileLocation = fileArray[i].key;
          // console.log('file nm', fileArray[i].key);
          // console.log('fileArray[i]', fileArray[i].key);
          images.push(fileLocation);
        }
        console.log('req : ', req.body);
        await authCheck.getUserId(
          req.headers.authorization,
          async (id, role, approve) => {
            await req.getConnection(async (err, connection) => {
              if (err) next(err);
              var sql = UPDATE_INFORMATION.code;
              await connection.query(
                sql,
                [
                  firstname,
                  lastname,
                  birth,
                  gender,
                  phone,
                  country,
                  city,
                  poscode,
                  address1,
                  address2,
                  national,
                  passport,
                  employeeStatus,
                  employeeAddress,
                  images[0],
                  images[1],
                  images[2],
                  images[3],
                  id
                ],
                async (err, result) => {
                  if (err) {
                    res.status(200).send(resp(false, err, null, null));
                  }
                  res
                    .status(200)
                    .send(
                      resp(
                        true,
                        result,
                        'success wait for approve',
                        'ทำรายการสำเร็จกรุณารอการยืนยัน'
                      )
                    );
                }
              );
            });
          }
        );
      }
    });
  } catch (err) {
    res.status(500).send({
      success: 'ERROR',
      message: err.message
    });
  }
};

exports.register = async (req, res, next) => {
  try {
    var { body } = req;
    let email = body.email;
    let password = body.password;
    let firstname = body.firstname;
    let lastname = body.lastname;
    let birth = body.birth;
    let gender = body.gender;
    let phone = body.phone;
    let country = body.country;
    let city = body.city;
    let poscode = body.poscode;
    let address1 = body.address1;
    let address2 = body.address2;
    let national = body.national;
    let passport = body.passport;
    let employeeStatus = body.employeeStatus;
    let employeeAddress = body.employeeAddress;
    let role = body.role;
    let refCode = body.refCode;
    var val = Math.floor(1000 + Math.random() * 9000);
    // console.log(body);

    // if(refCode == "" || refCode == null || refCode == false){
    //     refCode = 1
    // }

    if (role == '' || role == null || role == false) {
      role = 0;
    }

    const passwordHash = bcrypt.hashSync(password, 10);

    const errors = await validationResult(req);

    if (!errors.isEmpty()) {
      res.status(200).send(resp(false, errors.array(), null, null));
    } else {
      await req.getConnection(async (err, connection) => {
        if (err) return next(err);
        const varidateRef = await CheckRefId(connection, res, refCode);
        console.log('varidateRef : ', varidateRef);
        var checkRef = CHECK_REFID.code;
        var sql = FINE_BY_EMAIL.code;
        await connection.query(checkRef, [refCode], async (err, result) => {
          if (err) {
            return next(err);
          }

          if (result[0] || result.length > 0) {
            await connection.query(sql, [email], async (err, result) => {
              if (err) {
                return next(err);
              }
              if (result.length > 0) {
                return res
                  .status(200)
                  .send(
                    resp(
                      false,
                      errors.array(),
                      'Email is duplicate',
                      'อีเมลนี้ถูกใช้เรียบร้อย'
                    )
                  );
              } else {
                var sqlRegis = REGISTER_USER.code;
                await connection.query(
                  sqlRegis,
                  [
                    email,
                    passwordHash,
                    firstname,
                    lastname,
                    country,
                    refCode,
                    1,
                    val
                  ],
                  async (err, result2) => {
                    if (err) {
                      return next(err);
                    } else {
                      res
                        .status(200)
                        .send(
                          resp(
                            true,
                            result2,
                            'Register Success',
                            'สมัครสมาชิกเรียบร้อย'
                          )
                        );
                      return SendEmailVerify(email, val);
                      // return transporter.sendMail({
                      //     to:email,
                      //     from:'praprute@amaziefintech.com',
                      //     subject:'Signup succeeded! :)',
                      //     headings: 'Welcome To Amazie Fintech. The Future of Financial',
                      //     html:'sdsdsdsdsdsd'
                      // })
                    }
                  }
                );
              }
            });
          } else {
            res
              .status(200)
              .send(
                resp(
                  false,
                  result,
                  'Ref ID does not match',
                  'รหัสอ้างอิงไม่ถูกต้องกรุณาติดต่อ Broker ของท่าน'
                )
              );
          }
        });
      });
    }
  } catch (err) {
    res.status(500).send({
      success: 'ERROR',
      message: err.message
    });
  }
};

exports.requireSignin = expressJwt({
  secret: config.secret,
  userProperty: 'auth',
  algorithms: ['sha1', 'RS256', 'HS256']
});

exports.login = async (req, res, next) => {
  try {
    var { body } = req;
    let email = body.email;
    let password = body.password;
    const errors = await validationResult(req);

    if (!errors.isEmpty()) {
      res.status(200).send(resp(false, errors.array(), null, null));
    } else {
      await req.getConnection(async (err, connection) => {
        if (err) return next(err);
        var sql = FINE_BY_EMAIL.code;
        connection.query(sql, [email], (err, result) => {
          if (err) {
            return next(err);
          }
          if (result[0]) {
            const isCorrect = bcrypt.compareSync(password, result[0].Password);
            if (isCorrect) {
              const token = jwt.sign(
                {
                  id: result[0].id,
                  email: result[0].Email,
                  role: result[0].role,
                  approve: result[0].approve,
                  iat: Math.floor(new Date() / 1000)
                },
                config.secret
              );
              res.cookie('tokenShareExpire', token, {
                expire: new Date() + 9999
              });
              return res.status(200).send(
                resp(
                  true,
                  {
                    token: token,
                    role: result[0].role,
                    approve: result[0].approve
                    // timestamp:  result[0].RegisterTime,
                  },
                  'Login Success',
                  'เข้าสู่ระบบเรียบร้อย'
                )
              );
            } else {
              return res
                .status(200)
                .send(
                  resp(false, null, 'password not true', 'รหัสผ่านไม่ถูกต้อง')
                );
            }
          } else {
            return res
              .status(200)
              .send(
                resp(
                  false,
                  null,
                  'Pls registeration',
                  'กรุณาสมัครสมาชิกเพื่อเข้าใช้งาน'
                )
              );
          }
        });
      });
    }
  } catch (err) {
    res.status(500).send({
      success: 'ERROR',
      message: err.message
    });
  }
};

async function verifyByCode(verifyCode, dataCode) {
  if (verifyCode == dataCode) {
    return true;
  } else {
    return false;
  }
}

exports.getEmail = async (req, res, next) => {
  try {
    var { body } = req;
    var token = req.token;
    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      authCheck.getUserId(
        req.headers.authorization,
        async (id, role, approve) => {
          var sql = `SELECT email FROM AmazieFintechP1.user where id=${id};`;
          await connection.query(sql, (err, result) => {
            if (err) return next(err);
            res.status(200).send(resp(true, result[0], null, null));
          });
        }
      );
    });
  } catch (err) {
    res.status(500).send({
      success: 'ERROR',
      message: err.message
    });
  }
};

exports.VerifyByEmail = async (req, res, next) => {
  try {
    var { body } = req;
    let idUser = body.idUser;
    let email = body.email;
    let verifyCode = body.verifyCode;
    const errors = await validationResult(req);
    if (!errors.isEmpty()) {
      res.status(200).send(resp(false, errors.array(), null, null));
    } else {
      await req.getConnection(async (err, connection) => {
        if (err) return next(err);
        authCheck.getUserId(
          req.headers.authorization,
          async (id, role, approve) => {
            var sql = FINE_BY_ID.code;
            await connection.query(sql, [id], async (err, result) => {
              if (err) {
                return next(err);
              } else {
                const verify = await verifyByCode(
                  verifyCode,
                  result[0].verifyCode
                );

                if (verify) {
                  var UPDATEVERIFYBYEMAIL = UPDATE_VERIFY_BYEMAIL.code;
                  await connection.query(
                    UPDATEVERIFYBYEMAIL,
                    [2, id, result[0].Email],
                    async (err, result) => {
                      if (err) {
                        return next(err);
                      } else {
                        res
                          .status(200)
                          .send(
                            resp(
                              true,
                              null,
                              'verify code success',
                              'ยืนยันตัวตนผ่านอีเมลล์เรียบร้อย'
                            )
                          );
                      }
                    }
                  );
                } else {
                  res
                    .status(200)
                    .send(
                      resp(
                        false,
                        null,
                        `Please check verify code at ${email}`,
                        `กรุณาตรวจสอบรหัสผ่านที่ ${email} อีกครั้ง`
                      )
                    );
                }
              }
            });
          }
        );
      });
    }
  } catch (err) {
    res.status(500).send({
      success: 'ERROR',
      message: err.message
    });
  }
};

exports.reSendEmail = async (req, res, next) => {
  try {
    var { body } = req;
    let email = body.email;
    var val = Math.floor(1000 + Math.random() * 9000);
    const errors = await validationResult(req);
    if (!errors.isEmpty()) {
      res.status(200).send(resp(false, errors.array(), null, null));
    } else {
      await req.getConnection(async (err, connection) => {
        if (err) {
          return next(err);
        } else {
          authCheck.getUserId(
            req.headers.authorization,
            async (id, role, approve) => {
              var sql = CHANGE_VERIFY_CODE.code;
              await connection.query(sql, [val, id], async (err, result) => {
                if (err) {
                  return next(err);
                } else {
                  res
                    .status(200)
                    .send(
                      resp(
                        true,
                        null,
                        `Send verify code to ${email} success.`,
                        `ส่งรหัสยืนยันตัวตนไปที่ ${email} เรียบร้อย`
                      )
                    );
                  return SendEmailVerify(email, val);
                }
              });
            }
          );
        }
      });
    }
  } catch (err) {
    res.status(500).send({
      success: 'ERROR',
      message: err.message
    });
  }
};

exports.forgetPassword = async (req, res, next) => {
  try {
    var { body } = req;
    let email = body.email;
    var val = Math.floor(1000 + Math.random() * 9000);
    const errors = await validationResult(req);
    if (!errors.isEmpty()) {
      res.status(422).send(resp(false, errors.array(), null, null));
    } else {
      await req.getConnection(async (err, connection) => {
        if (err) {
          return next(err);
        } else {
          var sql = FINE_BY_EMAIL.code;
          connection.query(sql, [email], (err, result) => {
            if (err) {
              return next(err);
            } else {
              // console.log(result.length)
              if (result == [] || result.length == 0) {
                res.status(200).send({
                  success: 'error',
                  message: 'Dont Find your email',
                  info: result
                });
              } else {
                var sql2 = CHANGE_VERIFY_CODE_FORGET.code;
                connection.query(sql2, [val, email], async (err, result2) => {
                  if (err) {
                    return next(err);
                  } else {
                    res.status(200).send({
                      success: 'success',
                      message: 'Send Code to Your Email',
                      info: result
                    });
                    return SendEmailVerify(email, val);
                  }
                });
              }
            }
          });
        }
      });
    }
  } catch (err) {
    res.status(500).send({
      success: 'ERROR',
      message: err.message
    });
  }
};

exports.upDatePassword = async (req, res, next) => {
  try {
    var { body } = req;
    let email = body.email;
    let password = body.password;
    let verifyCode = body.verifyCode;
    const passwordHash = bcrypt.hashSync(password, 10);
    const errors = await validationResult(req);
    if (!errors.isEmpty()) {
      res.status(422).send({
        success: 'ERROR',
        message: errors.array()
      });
    } else {
      await req.getConnection(async (err, connection) => {
        if (err) {
          return next(err);
        } else {
          var sql = FINE_BY_EMAIL.code;
          connection.query(sql, [email], async (err, result) => {
            if (err) {
              return next(err);
            } else {
              const verify = await verifyByCode(
                verifyCode,
                result[0].verifyCode
              );
              if (!verify) {
                res.status(200).send({
                  success: 'error',
                  message: 'Verify Code dont accept. Pls Check Your Email',
                  info: result
                });
              } else {
                var updatePS = RESET_PASSWORD.code;
                connection.query(
                  updatePS,
                  [passwordHash, email],
                  async (err, result) => {
                    if (err) {
                      return next(err);
                    } else {
                      res.status(200).send({
                        success: 'success',
                        message: 'Update Success',
                        info: result
                      });
                    }
                  }
                );
              }
              // console.log('verify : ', verify)
              // console.log('email : ', result)
            }
          });
        }
      });
    }
  } catch (err) {
    res.status(500).send({
      success: 'ERROR',
      message: err.message
    });
  }
};

exports.signout = (req, res, next) => {
  res.clearCookie('tokenShareExpire');
  res.json({
    success: 'success',
    message: 'Signout Success'
  });
};

exports.approveKYC = async (req, res, next) => {
  try {
    var { body } = req;
    // console.log(body)
    let detail = body.details;
    let idUser = body.idUser;
    let accountnumber = body.accountnumber;
    let bankname      = body.bankname;
    let accountname   = body.accountname;
    let swiftcode     = body.swiftcode;
    let approved = body.approve;
    let stepapprove = 3
    if (approved == 1) {
      stepapprove = 4;
    }else{
      stepapprove = 3;
    }

    if (
      !detail ||
      detail == null ||
      detail.length == 0 ||
      detail == undefined
    ) {
      detail = '-';
    }

    await req.getConnection(async (err, connection) => {
      if (err) return next(err);
      await authCheck.getUserId(
        req.headers.authorization,
        async (id, role, approve) => {
          var sql = APPROVE_KYC.code;
          const date_now = moment(new Date(), 'Asia/Bangkok').format(
            'YYYY-MM-DD h:mm:ss'
          );
          if (role == 3) {
            await connection.query(
              sql,
              [
                accountnumber,
                bankname,
                accountname,
                swiftcode,
                date_now,
                stepapprove,
                approved,
                detail,
                idUser
              ],
              async (err, result) => {
                if (err) {
                  return next(err);
                } else {
                  res
                    .status(200)
                    .send(
                      resp(
                        true,
                        result,
                        `Approve User Success`,
                        `ยืนยันตัวตนลูกค้าเรียบร้อย`
                      )
                    );
                  // return approvekyc(email);
                }
              }
            );
          } else {
            res
              .status(200)
              .send(resp(false, null, `Permission denite`, `สถานะไม่ถูกต้อง`));
          }
        }
      );
    });
  } catch (err) {
    res.status(500).send({
      success: 'ERROR',
      message: err.message
    });
  }
};

exports.CheckAdmin = async (req, res, next) => {
  try {
    await authCheck.getUserId(
      req.headers.authorization,
      async (id, role, approve) => {
        if (role >= 3) {
          // res.status(200).send(resp(true, null, `Permission denite`, `สถานะไม่ถูกต้อง`))
          return next();
        } else {
          res
            .status(200)
            .send(resp(false, null, `Permission denite`, `สถานะไม่ถูกต้อง`));
        }
      }
    );
  } catch (err) {
    res.status(500).send({
      success: 'ERROR',
      message: err.message
    });
  }
};
