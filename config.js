const fs = require('fs');


module.exports = {
	secret: '4r0j959709ni62dasdfkaknkndfvkwe6783993h13bjsdcksjdvkawdf392832340192ekjfbkjfa8366612553078u478o',
	dbOptions: {
		host: process.env.DB_HOST,
		user: process.env.DB_USERNAME,
		password: process.env.DB_PASSWORD,
		port: process.env.DB_PORT,
		database: process.env.DB_DATABASE,
		dateStrings: true,
        ssl_mode: 'REQUIRED',
        dialect: 'mysql',
        logging: true,
        force: true,
        timezone: '+07:00',
        // sslmode = REQUIRED
		// insecureAuth: false,
        ssl:{
            ssl:true,
            ca:fs.readFileSync('ca-certificate.crt')
        }
	}
};

