const express = require('express');
const router = express.Router();
const { requireSignin } = require('../controllers/auth/AuthController')
const {
  dashboard,
  getDataADMIN,
  getUserDashboard,
  getUser,
  amountStock,
  transactionTopupDashboard,
  ApproveDivident,
  getPendingWithdraw,
  getImageSlip,
  UpdateWithDraw,
  ApproveCommission,
  ApproveWithDraw,
  ApproveTOPUP,
  WalletHistory,
  AllUserNetworkBB,
  getCommissionBroker
} = require('../controllers/FrontDashboard/DashboardController');
const {CheckAdmin} = require('../controllers/auth/AuthController')
router.get('/getUserDashboard', requireSignin, getUserDashboard)
router.get('/getUser', requireSignin, getUser)
router.get('/amountStock', requireSignin, amountStock)
router.get('/getPendingWithdraw', requireSignin, CheckAdmin,getPendingWithdraw);
router.get('/transactionTopupDashboard', requireSignin, transactionTopupDashboard)
router.get('/GET_INFORMATION_DASHBOARD', requireSignin, dashboard)
router.post('/ApproveDivident', requireSignin, CheckAdmin, ApproveDivident);
router.post('/ApproveCommission', requireSignin, CheckAdmin, ApproveCommission);
router.post('/ApproveWithDraw', requireSignin, CheckAdmin, ApproveWithDraw);
router.post('/ApproveTOPUP', requireSignin, CheckAdmin, ApproveTOPUP);
router.get('/admin/getData/:type', requireSignin, CheckAdmin, getDataADMIN);
router.get('/getWallet/:type', requireSignin, WalletHistory);
router.get('/getAllUserNetwork', requireSignin, AllUserNetworkBB);
router.get('/getCommissionBroker', requireSignin, getCommissionBroker);

router.get(
  '/getPendingWithdraw',
  requireSignin,
  CheckAdmin,
  getPendingWithdraw
);

router.post('/UpdateWithDraw', requireSignin, CheckAdmin, UpdateWithDraw);
router.get(
  '/getImageSlip/:path/:path2',
  
  getImageSlip
);
module.exports = router;