const express = require('express');
const router = express.Router();
const { body, param, check } = require('express-validator');

const {
  FetchCountry,
  updateKYC,
  FecthIndexKYC,
  getImageKYC,
  upload,
  requireSignin,
  getEmail,
  upDatePassword,
  register,
  login,
  signout,
  VerifyByEmail,
  reSendEmail,
  approveKYC,
  CheckAdmin,
  getImageFaceIMG,
  getImageBookbankIMG,
  getImageFrontPassportIMG,
  getImageBackPassportIMG,
  forgetPassword
} = require('../controllers/auth/AuthController');

router.post(
  '/signup',
  [
    body('email')
      .isEmail()
      .trim()
      // .withMessage('Please enter a valid Email')
      .exists({ checkNull: true, checkFalsy: true }),
    body('password')
      .isString()
      .isLength({ min: 8 })
      .withMessage('min 8')
      .isLength({ max: 20 })
      .withMessage('max 20')
      .isAlphanumeric()
      .withMessage('Please enter password with text and number'),
    body('confirmPassword').custom((value, { req }) => {
      if (value !== req.body.password) {
        throw new Error('Password dont match');
      }
      return true;
    })
  ],
  register
);

router.post(
  '/signin',
  [
    body('email')
      .isEmail()
      .withMessage('Please enter a valid Email')
      .trim()
      .exists({ checkNull: true, checkFalsy: true }),
    body('password')
      .isString()
      .isLength({ min: 6 })
      .withMessage('min 6')
      .isAlphanumeric()
      .withMessage('Please enter password with text and number')
      .trim()
  ],
  login
);

router.post('/VerifyByEmail', requireSignin, VerifyByEmail);

router.post('/ResendEmailVerify', requireSignin, reSendEmail);

router.post(
  '/forgetPassword',
  [
    body('email')
      .isEmail()
      .withMessage('Please enter a valid Email')
      .trim()
      .exists({ checkNull: true, checkFalsy: true })
  ],
  forgetPassword
);
// ResendEmailVerify

router.post(
  '/upDatePassword',
  [
    body('email')
      .isEmail()
      .withMessage('Please enter a valid Email')
      .trim()
      .exists({ checkNull: true, checkFalsy: true })
  ],
  upDatePassword
);
// ResendEmailVerify

router.get('/getEmail', requireSignin, getEmail);
// getImageKYC;
router.post('/getIndexKYC', requireSignin, CheckAdmin, FecthIndexKYC);
router.get('/getImageKYC/:path/:path2', getImageKYC);

//   getImageBookbankIMG,
//     getImageFrontPassportIMG,
//     getImageBackPassportIMG,
router.get('/getCountry', FetchCountry);

router.post('/UpdateIndexKYC', requireSignin, updateKYC);

router.post('/approveKYC', requireSignin, CheckAdmin, approveKYC);

// router.post('/upload', upload)

router.post('/signout', signout);

module.exports = router;
