const express = require('express');
const router = express.Router();
const { requireSignin } = require('../controllers/auth/AuthController')
const {
  getPlaned,
  BuyStock,
  SelectPlan,
  QueryInvestAmount,
  QueryTransactionTable,
  pendingTransaction
} = require('../controllers/Investment/InvestController');

// SelectPlan;

router.get('/getPlan', getPlaned)
router.post('/BuyStock', requireSignin, BuyStock);
router.post('/SelectPlan', SelectPlan)
router.get('/QueryInvestAmountCard', requireSignin,QueryInvestAmount);
router.get('/QueryTransactionTable', requireSignin, QueryTransactionTable);
router.get('/PendingTransaction', requireSignin, pendingTransaction);
// router.post()
module.exports = router;