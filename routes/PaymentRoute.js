const express = require('express');
const router = express.Router();
const { requireSignin } = require('../controllers/auth/AuthController')
const {SendSlipPayment,withDraw, OmiseWebHook, CheckOutCreditCard, CheckOutInnteretBanking} =  require('../controllers/payment/Payment')

router.post('/check-credit-card',  requireSignin,CheckOutCreditCard);
router.post('/transfer-slip',  requireSignin,SendSlipPayment);
router.post('/check-internet-banking', CheckOutInnteretBanking)
router.post('/banks-hooks', OmiseWebHook)
router.post('/withdraw', requireSignin, withDraw);
// hook-read
module.exports = router;

// Junior@492047


// curl https://api.omise.co/sources \
//   -u pkey_test_5pc4tfuajap242bduyg: \
//   -d "amount=400000" \
//   -d "currency=THB" \
//   -d "type=internet_banking_scb"
        // pkey_test_5pc4tfuajap242bduyg

//         curl https://api.omise.co/charges \
//   -u skey_test_5oriaz61s0xs6qbmlta: \
//   -d "amount=100000" \
//   -d "currency=thb" \
//   -d "return_uri=https://www.omise.co/example_return_uri" \
//   -d "source[type]=internet_banking_scb"

