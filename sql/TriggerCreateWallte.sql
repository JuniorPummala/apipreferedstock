
-- Create Trigger Topup  

-- DROP TRIGGER IF EXISTS `AmazieFintechP1`.update_user_balance;
-- DELIMITER $$
-- CREATE TRIGGER `AmazieFintechP1`.update_user_balance
-- 	AFTER UPDATE
--     ON `AmazieFintechP1`.transactionTopUp FOR EACH ROW
-- BEGIN
--     if new.type = 1 and new.status = 1 then
-- 	update `AmazieFintechP1`.user set user.balance = (user.balance+new.net) where user.id = new.idUser;
--     ELSEIF new.type = 2 and new.confirmWithdraw = 1 then
--     update `AmazieFintechP1`.user set user.balance = (user.balance-new.net) where user.id = new.idUser;
--     END IF;
-- END;
-- $$
-- DELIMITER ; 


-- Create Trigger Broker

-- DROP TRIGGER IF EXISTS `AmazieFintechP1`.update_user_to_broker;
-- DELIMITER $$
-- CREATE TRIGGER `AmazieFintechP1`.update_user_to_broker
-- 	AFTER UPDATE
--     ON `AmazieFintechP1`.user FOR EACH ROW
-- BEGIN
--     if new.role = 1 then
--     INSERT INTO `AmazieFintechP1`.`broker`
-- 	(`commission`,
-- 	`rank`,
-- 	`userID`)
-- 	VALUES
-- 	(14.28, 1, new.id);
-- 	END IF;
-- END;
-- $$
-- DELIMITER ; 


-- Add Trigger Broker add_user_share

-- DROP TRIGGER IF EXISTS `AmazieFintechP1`.add_user_share;
-- DELIMITER $$
-- CREATE TRIGGER `AmazieFintechP1`.add_user_share
-- 	AFTER INSERT
--     ON `AmazieFintechP1`.transactionInvestment FOR EACH ROW
-- BEGIN
--     if new.statusStock = 1 then
-- 	update `AmazieFintechP1`.user set user.share = new.share, user.balance = (user.balance-new.amount) where user.id = new.idUser;
--     END IF;
-- END;
-- $$
-- DELIMITER ; 


