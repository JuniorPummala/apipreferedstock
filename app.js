const express      = require('express')
const app          = express()
const cors         = require('cors')
const bodyParser   = require('body-parser')
const mysql        = require('mysql')
const myConnection = require('express-myconnection')
require('dotenv').config()
const fs = require('fs');
const dbOptions = require('./config')
const authRoutes = require('./routes/AuthRoute')
const payMent = require('./routes/PaymentRoute') 
const InvestPlan = require('./routes/IvestRoute')
const Dashboard = require('./routes/DashboardRoute')
var moment = require('moment-timezone');
const cron = require('node-cron');

const PORT = 8004;

var omise = require('omise')({
    'publicKey':process.env.PIBLIC_KEY_OMISE,
    'secretKey':process.env.SECRET_KEY_OMISE
})

app.use(cors({
    "origin": "*",
    "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
    "preflightContinue": false,
    "optionsSuccessStatus": 204
  }))

app.use(bodyParser.json());

app.use(express.json())

var connection = mysql.createConnection({
	host: process.env.DB_HOST,
	user: process.env.DB_USERNAME,
	password: process.env.DB_PASSWORD,
	port: process.env.DB_PORT,
	database: process.env.DB_DATABASE,
	dateStrings: true,
    ssl_mode: 'REQUIRED',
    dialect: 'mysql',
    logging: true,
    force: true,
    timezone: '+07:00',
    ssl:{
            ssl:true,
            ca:fs.readFileSync('ca-certificate.crt')
        }
});

connection.connect((err) => {
    if(err){
     return console.log(`error : `, err)
    }else{
     return console.log('Connect Database Success')
    }
})

app.use(myConnection(mysql, dbOptions.dbOptions, "pool"));

app.use('/api', authRoutes);
app.use('/api', payMent);
app.use('/api', InvestPlan);
app.use('/api', Dashboard);

const date_now = moment(new Date(), 'Asia/Bangkok').add(1, 'y').format('YYYY-MM-DD h:mm:ss')
console.log('date_now : ', date_now);


app.listen(PORT, () => {
    console.log("ready server on http://localhost:" + PORT);
});

cron.schedule('*/5 * * * *', async () => {
  console.log('Task is running every minute ' + new Date());
});

//   * * * * * *
//   | | | | | |
//   | | | | | day of week
//   | | | | month
//   | | | day of month
//   | | hour
//   | minute
//   second ( optional )









// SELECT * FROM `order` WHERE date_format(order_date, '%y%m') = '2101';


// SELECT `order`.order_id, bill_id, order_date, CONCAT(first_name, ' ', family_name), order_sum_price, product_name
// FROM `order` 
// LEFT JOIN member_profile ON member_profile.member_id = `order`.member_id
// LEFT JOIN product_stock_out ON product_stock_out.order_id = `order`.order_id
// LEFT JOIN product ON product.product_id = product_stock_out.product_id
// WHERE DATE(order_date) = '2021-01-09'  AND order_payment = 12000;
// -- WHERE (DATE(order_date) BETWEEN '2021-01-07' AND '2021-01-11') AND order_payment = 12000;
